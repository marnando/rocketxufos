﻿using UnityEngine;
using System.Collections;

public class InimigoScript : MonoBehaviour {

	public ArmaScript arma;
	public Animator animation;

	void Awake() {
		arma = GetComponent<ArmaScript> ();
	}

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 5);
	}
	
	// Update is called once per frame
	void Update () {
		if (arma != null && arma.podeAtacar) {
			arma.atacar(true);
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
		animation.SetBool("explode", true);
		//yield return new WaitForSeconds(delayTime);
	}
}
